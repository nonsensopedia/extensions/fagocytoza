<?php

use MediaWiki\MediaWikiServices;

class FagocytozaHooks {
	private const WSG_PREF_SIDEBAR_LINK = 'fagocytoza-wsg-sidebar';

	/**
	 * Adds a link in the toolbox to Special:WolneStronyGry
	 *
	 * @param Skin $skin
	 * @param $bar
	 *
	 * @throws MWException
	 */
	public static function onSidebarBeforeOutput( Skin $skin, &$bar ) {
		$optionsLookup = MediaWikiServices::getInstance()->getUserOptionsLookup();
		if ( !$optionsLookup->getBoolOption( $skin->getUser(), self::WSG_PREF_SIDEBAR_LINK ) ) {
			return;
		}

		$bar['TOOLBOX'][] = [
			'text' => $skin->msg( 'wolne_strony_gry' ),
			'href' => SpecialPage::getTitleFor( 'Wolne_strony_Gry' )->getFullURL()
		];
	}

	public static function onGetPreferences( $user, &$preferences ) {
		$preferences[self::WSG_PREF_SIDEBAR_LINK] = [
			'type' => 'check',
			'label-message' => 'fagocytoza-wsg-sidebar-pref',
			'section' => 'rendering/nonsa',
		];
	}
}